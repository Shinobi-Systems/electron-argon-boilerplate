var loadLib = function(lib){
    return require(__dirname+'/libs/'+lib+'.js')
}
var s = loadLib('process')(process,__dirname)
var config = loadLib('config')(s)
loadLib('basic')(s,config)
loadLib('webServer')(s,config)
loadLib('electron')(s,config)
var firstParameter = process.argv[2]
if(firstParameter){
    console.log('First Parameter Detected',firstParameter)
}
module.exports = s
