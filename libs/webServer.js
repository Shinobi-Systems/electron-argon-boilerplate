var express = require('express')
var http = require('http')
var app = express()
var io = new (require('socket.io'))()
var bodyParser = require('body-parser')
var exec = require('child_process').exec
module.exports = function(s,config){
    // HTTP Config
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    // HTTP Handles
    app.use(function (req,res,next){
        res.setHeader('Content-Type', 'application/json')
        next()
    })
    app.get('/', function (req,res){
        var endData = {}
        res.end(s.prettyPrint(endData))
    })
    var server = http.createServer(app)
    server.listen(config.webPanelPort,null,function(){
        console.log(`Shinobi Electron Boilerplate running on port ${config.webPanelPort}!`)
    })
    // Socket.IO Handles (WebSocket)
    io.attach(server,{
        transports: ['websocket']
    })
    io.on('connection', function(socket){
        console.log('Connected User')
    })
    s.sendToConnectedClients = function(data){
        io.emit('f',data)
    }
}
