window.config = ejsData.config
const shell = require('electron').shell;
var webHost = 'http://127.0.0.1:' + ejsData.webPort + '/'
var getJSON = function(pointer,callback){
    $.get(webHost + pointer,function(data){
        callback(data)
    })
}
$(document).ready(function(){
    $('body')
    .on('click','[get-json]',function(){
        var pointer = $(this).attr('get-json')
        getJSON(pointer,function(data){
            console.log(pointer,data)
        })
    })
    .on('click','[click-to-copy]',function(e){
        var pointer = $(this).attr('click-to-copy')
        var text
        switch(pointer){
            case'this':
                text = $(this).text()
            break;
            case'thisHtml':
                text = $(this).html()
            break;
            case'href':
                e.preventDefault()
                text = $(this).attr('href')
            break;
        }
        var $temp = $("<input>")
        $("body").append($temp)
        $temp.val(text).select()
        document.execCommand("copy")
        $temp.remove()
    })
})
$(document).on('click', 'a[href^="http"]', function(event) {
    event.preventDefault()
    shell.openExternal(this.href)
})
// Socket.IO Handles (WebSocket)
var webSocket = io(webHost,{
    transports: ['websocket'],
    forceNew: false
})
webSocket.on('f',function(data){
    switch(data.f){
        case'onStart':
            console.log(data)
        break;
    }
})
