module.exports = function(s){
    var config
    try{
        config = require(s.mainDirectory + '/conf.json')
    }catch(err){
        if(err && err.toString().indexOf('Cannot find module') === -1){
            console.log(err.toString())
            console.log('Your conf.json syntax may be broken.')
        }
        console.log('Default configuration will be used.')
        config = {}
    }
    var defaultConfig = {
        "webPanelPort": 9511,
    }
    Object.keys(defaultConfig).forEach(function(configKey){
        if(config[configKey] === undefined)config[configKey] = defaultConfig[configKey]
    })
    s.configuration = config
    return config
}
