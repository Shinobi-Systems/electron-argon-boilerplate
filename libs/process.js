module.exports = function(process,dirname){
    process.on("uncaughtException", function(error) {
        console.error(error)
    })
    return {
        isWindows: (process.platform === 'win32' || process.platform === 'win64'),
        mainDirectory: dirname
    }
}
